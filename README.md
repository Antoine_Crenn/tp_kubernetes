## Groupe Laurent Maréchal et Antoine Crenn

# TP Kubernetes

## Namespaces

Notre découpage des namespaces est le namespace devops303 qui contient 2 pods devops303, un pod redis, il y a aussi le namespace hello qui est constitué de 3 pods hello-world. Le namespace hello a un quota pour limiter son impact sur les ressources du cluster et le namespace devops303 a aussi un quota pur limiter son utilisation des ressources.

## Devops303, Redis et Hello-world

Dans le namespace devops303, on a mis en place 2 pods Devops303 pour que le service soit redondant et un pod Redis que l'on a lié aux pods Devops303 grâce à des variables d'environnement présent dans le fichier ConfigMap. Ces variables d'environnement sont le redis port et le redis host. Dans le namespace hello, on a mis en place 3 pods Hello-World. Et les pods Devops303 et hello-world sont respectivement joiniables avec devops303.antoine suivi du port et hello.antoine suivi du port.

## Monitoring

Pour le monitoring, on a réalisé l'installation avec la commande `helm install prometheus prometheus-community/kube-prometheus-stack`. Puis pour lancer Grafana, nous avons fait la commande suivante `kubectl port-forward deployment/prometheus-grafana 3000` ce qui permet de réaliser un port-forward du service Grafana par le port 3000, ce qui permet de taper localhost:3000 sur un navigateur et d'obtenir une page de connexion pour Grafana. Sur cette page les identifiants sont admin et prom-operator, ce qui permet d'obtenir un dashbord grâce aux métriques récupérées par Prometheus.

![](https://i.imgur.com/DzzbBK2.png)

Et nous avons relié les alertes Prometheus à un salon textuel Discord grâce à Webhooks. Ce qui nous permet de facilement visualiser s'il y a une alerte liée au cluster.

## RBAC

Nous avons créé 3 rôles qui sont admin-cluster, dev et client. Les droits d'admin-cluster sont qu'il a tous les droits. Pour dev, on lui a juste enlevé les droits de suppression par rapport à admin-cluster et client n'a que les droits de lecture à partir d'un port-forward. Mais nous n'avons pas compris comment identifier si nos rôles étaient entièrement fonctionnels.

On a aussi mis en place un label criticity:danger sur les ressources du cluster Devops303 pour que les ressources du namespace ne soient pas modifiées par toutes les personnes.

## KubeDB

Nous avons installé KubeDB avec la commande suivante `helm install kubedb appscode/kubedb`, mais nous n’avons pas compris comment associer KubeDB aux données qui provient de redis.
